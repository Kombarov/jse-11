package ru.kombarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printProjects;

public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        if (serviceLocator == null) throw new Exception();
        printProjects(serviceLocator.getProjectEndpoint().findAllProjectsByUserId(serviceLocator.getSession()));
        System.out.println("[OK]");
    }
}
