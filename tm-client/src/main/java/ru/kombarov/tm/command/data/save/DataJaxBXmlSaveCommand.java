package ru.kombarov.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

public class DataJaxBXmlSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data JaxB xml save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data by JaxB to XML format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JAXB XML SAVE]");
        if (serviceLocator == null) return;
        serviceLocator.getUserEndpoint().saveDataXmlByJaxB(serviceLocator.getSession());
        System.out.println("[OK]");
    }
}
