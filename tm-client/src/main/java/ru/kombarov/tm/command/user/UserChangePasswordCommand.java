package ru.kombarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.User;
import ru.kombarov.tm.command.AbstractCommand;

public final class UserChangePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-change password";
    }

    @NotNull
    @Override
    public String description() {
        return "Change user password.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD");
        final @Nullable String password = input.readLine();
        @Nullable User user = new User();
        if (serviceLocator == null) throw new Exception();
        user = serviceLocator.getUserEndpoint().findOneUser(serviceLocator.getSession(), serviceLocator.getSession().getUserId());
        if (user != null) user.setPassword(password);
        serviceLocator.getUserEndpoint().mergeUser(serviceLocator.getSession(), user);
        System.out.println("[OK]");
    }
}
