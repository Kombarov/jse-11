package ru.kombarov.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

public class DataBinSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data bin save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to binary format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BIN SAVE]");
        if (serviceLocator == null) return;
        serviceLocator.getUserEndpoint().saveDataBin(serviceLocator.getSession());
        System.out.println("[OK]");
    }
}
