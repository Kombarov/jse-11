package ru.kombarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.Session;
import ru.kombarov.tm.command.AbstractCommand;

public final class UserLoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Login.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOG IN]");
        System.out.println("ENTER LOGIN");
        @NotNull String login = input.readLine();
        System.out.println("ENTER PASSWORD");
        @NotNull String password = input.readLine();
        @Nullable Session session = serviceLocator.getSessionEndpoint().createSession(login, password);
        while(session == null) {
            System.out.println("Invalid login or password");
            System.out.println("ENTER LOGIN");
            login = input.readLine();
            System.out.println("ENTER PASSWORD");
            password = input.readLine();
            session = serviceLocator.getSessionEndpoint().createSession(login, password);
        }
        serviceLocator.setSession(session);
        System.out.println("[OK]");
    }
}
