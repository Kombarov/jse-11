package ru.kombarov.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.api.endpoint.Exception_Exception;
import ru.kombarov.tm.command.AbstractCommand;

public class DataBinLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data bin load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from binary format.";
    }

    @Override
    public void execute() throws Exception_Exception {
        System.out.println("[DATA BIN LOAD]");
        if (serviceLocator == null) return;
        serviceLocator.getUserEndpoint().loadDataBin(serviceLocator.getSession());
        System.out.println("[OK]");
    }
}
