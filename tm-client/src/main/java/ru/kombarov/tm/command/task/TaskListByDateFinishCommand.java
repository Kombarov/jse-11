package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printTasks;

public class TaskListByDateFinishCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list show by finish date";
    }

    @NotNull
    @Override
    public String description() {
        return "Show tasks by finish date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST BY FINISH DATE]");
        if (serviceLocator == null) throw new Exception();
        printTasks(serviceLocator.getTaskEndpoint().sortTasksByDateFinish(serviceLocator.getSession()));
        System.out.println("[OK]");
    }
}
