package ru.kombarov.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

public class DataFasterXmlSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data fasterXml save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data by FasterXML to XML format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA FASTERXML SAVE]");
        if (serviceLocator == null) return;
        serviceLocator.getUserEndpoint().saveDataXmlByFasterXml(serviceLocator.getSession());
        System.out.println("[OK]");
    }
}
