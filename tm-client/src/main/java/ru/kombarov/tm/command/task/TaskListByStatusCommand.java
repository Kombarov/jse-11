package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.api.endpoint.Status;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.util.EntityUtil;

public class TaskListByStatusCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list show by status";
    }

    @NotNull
    @Override
    public String description() {
        return "Show tasks by status.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST BY STATUS]");
        System.out.println(Status.PLANNED.value() + ":");
        if (serviceLocator == null) throw new Exception();
        EntityUtil.printTasks(serviceLocator.getTaskEndpoint().sortTasksByStatus(serviceLocator.getSession(), Status.PLANNED));
        System.out.println("\n" + Status.DONE.value() + ":");
        EntityUtil.printTasks(serviceLocator.getTaskEndpoint().sortTasksByStatus(serviceLocator.getSession(), Status.DONE));
        System.out.println("\n" + Status.IN_PROCESS.value() + ":");
        EntityUtil.printTasks(serviceLocator.getTaskEndpoint().sortTasksByStatus(serviceLocator.getSession(), Status.IN_PROCESS));
        System.out.println("[OK]");
    }
}
