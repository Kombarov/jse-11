package ru.kombarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.*;

public final class UserViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-view";
    }

    @NotNull
    @Override
    public String description() {
        return "Show user info.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER VIEW]");
        if (serviceLocator == null) throw new Exception();
        final @NotNull String userId = serviceLocator.getSession().getUserId();
        printUser(serviceLocator.getUserEndpoint().findOneUser(serviceLocator.getSession(), userId));
        printProjects(serviceLocator.getProjectEndpoint().findAllProjectsByUserId(serviceLocator.getSession()));
        System.out.println();
        System.out.println("tasks:");
        printTasks(serviceLocator.getTaskEndpoint().findAllTasksByUserId(serviceLocator.getSession()));
        System.out.println("[OK]");
    }
}
