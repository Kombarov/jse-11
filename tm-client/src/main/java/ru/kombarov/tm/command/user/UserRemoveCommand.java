package ru.kombarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.Role;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printUsers;

public final class UserRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove existing user";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (serviceLocator.getSession().getRole().equals(Role.ADMINISTRATOR)) {
            System.out.println("[USER REMOVE]");
            printUsers(serviceLocator.getUserEndpoint().findAllUsers(serviceLocator.getSession()));
            System.out.println("TYPE USER NAME TO REMOVE");
            final @Nullable String name = input.readLine();
            final @Nullable String userId = serviceLocator.getUserEndpoint().getIdByUserName(serviceLocator.getSession(), name);
            serviceLocator.getUserEndpoint().removeUser(serviceLocator.getSession(), userId);
            System.out.println("[OK]");
        }
        else System.out.println("ACCESS IS DENIED");
    }
}
