package ru.kombarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.Project;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.DateUtil.parseStringToDate;
import static ru.kombarov.tm.util.EntityUtil.printProjects;

public final class ProjectEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit selected project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT EDIT]");
        if (serviceLocator == null) throw new Exception();
        printProjects(serviceLocator.getProjectEndpoint().findAllProjectsByUserId(serviceLocator.getSession()));
        System.out.println("ENTER PROJECT NAME FOR EDIT");
        final @Nullable String nameAnotherProject = input.readLine();
        final @NotNull Project anotherProject = new Project();
        anotherProject.setName(nameAnotherProject);
        System.out.println("ENTER PROJECT DESCRIPTION");
        anotherProject.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        anotherProject.setDateStart(parseStringToDate(input.readLine()));
        System.out.println("ENTER FINISH DATE");
        anotherProject.setDateFinish(parseStringToDate(input.readLine()));
        anotherProject.setId(serviceLocator.getProjectEndpoint().getIdByProjectName(serviceLocator.getSession(), nameAnotherProject));
        anotherProject.setUserId(serviceLocator.getSession().getUserId());
        serviceLocator.getProjectEndpoint().mergeProject(serviceLocator.getSession(), anotherProject);
        System.out.println("[OK]");
    }
}
