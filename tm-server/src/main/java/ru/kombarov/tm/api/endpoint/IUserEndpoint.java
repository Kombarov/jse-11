package ru.kombarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod
    void persistUser(final @NotNull String login, final @NotNull String password, final @NotNull Role role) throws Exception;

    @WebMethod
    void persistUsers(final @Nullable Session session, final @Nullable List<User> users) throws Exception;

    @WebMethod
    void mergeUser(final @Nullable Session session, final @Nullable User user) throws Exception;

    @NotNull
    @WebMethod
    List<User> findAllUsers(final @Nullable Session session) throws Exception;

    @Nullable
    @WebMethod
    User findOneUser(final @Nullable Session session, final @Nullable String id) throws Exception;

    @WebMethod
    void removeUser(final @Nullable Session session, final @Nullable String id) throws Exception;

    @WebMethod
    void removeAllUsers(final @Nullable Session session) throws Exception;

    @Nullable
    @WebMethod
    String getIdByUserName(final @Nullable Session session, final @Nullable String name) throws Exception;

    @NotNull
    @WebMethod
    List<User> findAllUsersByUserId(final @Nullable Session session, final @Nullable String userId) throws Exception;

    @Nullable
    @WebMethod
    User findOneUserByUserId(final @Nullable Session session, final @Nullable String userId, final @Nullable String id) throws Exception;

    @WebMethod
    void removeAllUsersByUserId(final @Nullable Session session, final @Nullable String userId) throws Exception;

    @WebMethod
    boolean checkUserLogin(final @Nullable Session session, final @Nullable String login) throws Exception;

    @WebMethod
    void loadDataBin(final @Nullable Session session) throws Exception;

    @WebMethod
    void loadDataJsonByFasterXml(final @Nullable Session session) throws Exception;

    @WebMethod
    void loadDataXmlByFasterXml(final @Nullable Session session) throws Exception;

    @WebMethod
    void loadDataJsonByJaxB(final @Nullable Session session) throws Exception;

    @WebMethod
    void loadDataXmlByJaxB(final @Nullable Session session) throws Exception;

    @WebMethod
    void saveDataBin(final @Nullable Session session) throws Exception;

    @WebMethod
    void saveDataJsonByFasterXml(final @Nullable Session session) throws Exception;

    @WebMethod
    void saveDataXmlByFasterXml(final @Nullable Session session) throws Exception;

    @WebMethod
    void saveDataJsonByJaxB(final @Nullable Session session) throws Exception;

    @WebMethod
    void saveDataXmlByJaxB(final @Nullable Session session) throws Exception;
}
