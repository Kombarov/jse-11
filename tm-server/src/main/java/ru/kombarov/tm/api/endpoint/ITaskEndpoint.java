package ru.kombarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    void persistTask(final @NotNull Session session, final @Nullable Task task) throws Exception;

    @WebMethod
    void persistTasks(final @NotNull Session session, final @Nullable List<Task> tasks) throws Exception;

    @WebMethod
    void mergeTask(final @NotNull Session session, final @Nullable Task task) throws Exception;

    @NotNull
    @WebMethod
    List<Task> findAllTasks(final @NotNull Session session) throws Exception;

    @Nullable
    @WebMethod
    Task findOneTask(final @NotNull Session session, final @Nullable String id) throws Exception;

    @WebMethod
    void removeTask(final @NotNull Session session, final @Nullable String id) throws Exception;

    @WebMethod
    void removeAllTasks(final @NotNull Session session) throws Exception;

    @Nullable
    @WebMethod
    String getIdByTaskName(final @NotNull Session session, final @Nullable String name) throws Exception;

    @NotNull
    @WebMethod
    List<Task> getTaskListByProjectId(final @NotNull Session session, final @Nullable String projectId) throws Exception;

    @NotNull
    @WebMethod
    List<Task> getTaskListByUserId(final @NotNull Session session) throws Exception;

    @NotNull
    @WebMethod
    List<Task> findAllTasksByUserId(final @NotNull Session session) throws Exception;

    @Nullable
    @WebMethod
    Task findOneTaskByUserId(final @NotNull Session session, final @Nullable String id) throws Exception;

    @WebMethod
    void removeAllTasksByUserId(final @NotNull Session session) throws Exception;

    @NotNull
    @WebMethod
    List<Task> sortTasksByDateStart(final @NotNull Session session) throws Exception;

    @NotNull
    @WebMethod
    List<Task> sortTasksByDateFinish(final @NotNull Session session) throws Exception;

    @NotNull
    @WebMethod
    List<Task> sortTasksByStatus(final @NotNull Session session, final @NotNull Status status) throws Exception;

    @NotNull
    @WebMethod
    List<Task> findTasksByPart(final @NotNull Session session, final @Nullable String part) throws Exception;
}
