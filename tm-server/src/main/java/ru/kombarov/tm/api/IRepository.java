package ru.kombarov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<T> {

    @NotNull
    List<T> findAll();

    @NotNull
    List<T> findAll(final @NotNull String userId) throws Exception;

    @Nullable
    T findOne(final @NotNull String id);

    @Nullable
    T findOne(final @NotNull String userId, final @NotNull String id) throws Exception;

    void persist(final @NotNull T t) throws Exception;

    void persist(final @NotNull List<T> t);

    void merge(final @NotNull T t) throws Exception;

    void remove(final @NotNull String id) throws Exception;

    void removeAll();

    void removeAll(final @NotNull String userId) throws Exception;

    @Nullable
    String getIdByName(final @NotNull String name) throws Exception;
}
