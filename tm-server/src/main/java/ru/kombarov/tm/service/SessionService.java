package ru.kombarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.repository.SessionRepository;

public final class SessionService {

    private final @NotNull SessionRepository sessionRepository;

    public SessionService(final @NotNull SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    @Nullable
    public Session findOne(@Nullable final String userId, @Nullable final String id) throws Exception{
        if(userId == null || userId.isEmpty()) throw new Exception("Invalid user id");
        if(id == null || id.isEmpty()) throw new Exception("Invalid session id");
        return sessionRepository.findOne(userId, id);
    }

    public void remove(@Nullable String userId, @Nullable String id) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("Invalid user id");
        if(id == null || id.isEmpty()) throw new Exception("Invalid session id");
        sessionRepository.remove(userId, id);
    }

    public void persist(final @Nullable Session session) throws Exception {
        if (session == null) throw new Exception();
        else sessionRepository.persist(session);
    }
}
