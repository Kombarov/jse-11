package ru.kombarov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.IService;
import ru.kombarov.tm.constant.DataConstant;
import ru.kombarov.tm.datatransfer.DataTransfer;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.repository.AbstractRepository;
import ru.kombarov.tm.repository.UserRepository;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.util.List;

public final class UserService extends AbstractService<User> implements IService<User> {

    @Nullable
    private User userCurrent;

    @NotNull
    private final ProjectService projectService;

    @NotNull
    private final TaskService taskService;

    @NotNull
    private final UserRepository projectRepository = (UserRepository) abstractRepository;

    public UserService(final @NotNull AbstractRepository<User> abstractRepository, final @NotNull ProjectService projectService, final @NotNull TaskService taskService) {
        super(abstractRepository);
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Nullable
    @Override
    public String getIdByName(final @Nullable String name) throws Exception {
        if (name == null || name.isEmpty()) return null;
        else return abstractRepository.getIdByName(name);
    }

    @NotNull
    @Override
    public List<User> findAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        return abstractRepository.findAll(userId);
    }

    @Nullable
    @Override
    public User findOne(final @Nullable String userId, final @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        else return abstractRepository.findOne(userId, id);
    }

    @Override
    public void removeAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        abstractRepository.removeAll(userId);
    }

    public void setUserCurrent(final @Nullable User user) {
        userCurrent = user;
    }

    @Nullable
    public User getUserCurrent() {
        return userCurrent;
    }

    public boolean checkLogin(final @Nullable String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        return projectRepository.checkLogin(login);
    }

    public void loadDataBin() throws Exception {
        @NotNull
        final File file = new File(DataConstant.FILE_BINARY);
        @NotNull
        final FileInputStream fileInputStream = new FileInputStream(file);
        @NotNull
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull
        final DataTransfer dataTransfer = (DataTransfer) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        persist(dataTransfer.getUsers());
        projectService.persist(dataTransfer.getProjects());
        taskService.persist(dataTransfer.getTasks());
    }

    public void loadDataJsonByFasterXml() throws Exception {
        @NotNull
        final File file = new File(DataConstant.FILE_JSON);
        @NotNull
        final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull
        final DataTransfer dataTransfer = objectMapper.readValue(file, DataTransfer.class);
        persist(dataTransfer.getUsers());
        projectService.persist(dataTransfer.getProjects());
        taskService.persist(dataTransfer.getTasks());
    }

    public void loadDataXmlByFasterXml() throws Exception {
        @NotNull
        final File file = new File(DataConstant.FILE_XML);
        @NotNull
        final XmlMapper xmlMapper = new XmlMapper();
        @NotNull
        final DataTransfer dataTransfer = xmlMapper.readValue(file, DataTransfer.class);
        persist(dataTransfer.getUsers());
        projectService.persist(dataTransfer.getProjects());
        taskService.persist(dataTransfer.getTasks());
    }

    public void loadDataJsonByJaxB() throws Exception {
        @NotNull
        final File file = new File(DataConstant.FILE_JSON);
        @NotNull
        final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull
        final DataTransfer dataTransfer = objectMapper.readValue(file, DataTransfer.class);
        persist(dataTransfer.getUsers());
        projectService.persist(dataTransfer.getProjects());
        taskService.persist(dataTransfer.getTasks());
    }

    public void loadDataXmlByJaxB() throws Exception {
        @NotNull
        final File file = new File(DataConstant.FILE_XML);
        @NotNull
        final JAXBContext jaxbContext = JAXBContext.newInstance(DataTransfer.class);
        @NotNull
        final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull
        final DataTransfer dataTransfer = (DataTransfer) unmarshaller.unmarshal(file);
        persist(dataTransfer.getUsers());
        projectService.persist(dataTransfer.getProjects());
        taskService.persist(dataTransfer.getTasks());
    }

    public void saveDataBin() throws Exception {
        @NotNull
        final DataTransfer dataTransfer = new DataTransfer();
        dataTransfer.load(projectService, taskService, this);
        @NotNull
        final File file = new File(DataConstant.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(dataTransfer);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    public void saveDataJsonByFasterXml() throws Exception {
        @NotNull
        final DataTransfer dataTransfer = new DataTransfer();
        dataTransfer.load(projectService, taskService, this);
        @NotNull
        final File file = new File(DataConstant.FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, dataTransfer);
    }

    public void saveDataXmlByFasterXml() throws Exception {
        @NotNull
        final DataTransfer dataTransfer = new DataTransfer();
        dataTransfer.load(projectService, taskService, this);
        @NotNull
        final File file = new File(DataConstant.FILE_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull
        final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(file, dataTransfer);
    }

    public void saveDataJsonByJaxB() throws Exception {
        @NotNull
        final DataTransfer dataTransfer = new DataTransfer();
        dataTransfer.load(projectService, taskService, this);
        @NotNull
        final File file = new File(DataConstant.FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JaxbAnnotationModule());
        objectMapper.writeValue(file, dataTransfer);
    }

    public void saveDataXmlByJaxB() throws Exception {
        @NotNull
        final DataTransfer dataTransfer = new DataTransfer();
        dataTransfer.load(projectService, taskService, this);
        @NotNull
        final File file = new File(DataConstant.FILE_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull
        final JAXBContext jaxbContext = JAXBContext.newInstance(DataTransfer.class);
        @NotNull
        final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(dataTransfer, file);
    }
}