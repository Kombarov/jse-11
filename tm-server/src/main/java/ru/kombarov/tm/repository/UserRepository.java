package ru.kombarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.IRepository;
import ru.kombarov.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IRepository<User> {

    @NotNull
    @Override
    public List<User> findAll(final @NotNull String userId) {
        final @NotNull List<User> list = new ArrayList<User>(entityMap.values());
        return list;
    }

    @Nullable
    @Override
    public User findOne(final @NotNull String userId, final @NotNull String id) {
        return entityMap.get(id);
    }

    @Override
    public void removeAll(final @NotNull String userId) {
        entityMap.clear();
    }

    @Nullable
    @Override
    public String getIdByName(final @NotNull String name) throws Exception {
        final @NotNull List<User> users = findAll();
        for (final @NotNull User user : users) {
            if (user.getLogin() == null || user.getLogin().isEmpty()) throw new Exception();
            if (user.getLogin().equals(name)) return user.getId();
        }
        return null;
    }

    public boolean checkLogin(final @NotNull String login) throws Exception {
        for (final @NotNull User user : findAll()) {
            if (user.getLogin() == null || user.getLogin().isEmpty()) throw new Exception();
            if (user.getLogin().equals(login)) return true;
        }
        return false;
    }
}
