package ru.kombarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.IRepository;
import ru.kombarov.tm.entity.AbstractEntity;

import java.util.*;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    protected @NotNull Map<String, T> entityMap = new LinkedHashMap<>();

    @Override
    public void persist(final @NotNull T t) {
        entityMap.put(t.getId(), t);
    }

    @Override
    public void persist(@NotNull List<T> ts) {
        for (final @NotNull T t : ts) {
            entityMap.put(t.getId(), t);
        }
    }

    @Override
    public void merge(final @NotNull T t) {
        if (entityMap.containsKey(t.getId())) {
            entityMap.remove(t.getId());
            entityMap.put(t.getId(), t);
        }
        else {
            entityMap.put(t.getId(), t);
        }
    }

    @NotNull
    @Override
    public List<T> findAll() {
        final @NotNull List<T> list = new ArrayList<T>(entityMap.values());
        return list;
    }

    @Nullable
    @Override
    public T findOne(final @NotNull String id) {
        return entityMap.get(id);
    }

    @Override
    public void remove(final @NotNull String id) {
        entityMap.remove(id);
    }

    @Override
    public void removeAll() {
        entityMap.clear();
    }
}
