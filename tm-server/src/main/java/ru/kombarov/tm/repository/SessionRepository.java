package ru.kombarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Session;

import java.util.LinkedHashMap;
import java.util.Map;

public final class SessionRepository {

    protected @NotNull Map<String, Session> sessionMap = new LinkedHashMap<>();

    @Nullable
    public Session findOne(final @NotNull String userId, @NotNull final String id) {
        for(final @NotNull Session session : sessionMap.values()) {
            if(id.equals(session.getId()) && userId.equals(session.getUserId())) return session;
        }
        return null;
    }

    public void remove(@NotNull String userId, @NotNull String id) throws Exception {
        for(final @NotNull Session session : sessionMap.values()) {
            if(id.equals(session.getId()) && userId.equals(session.getUserId())) sessionMap.remove(id);
        }
    }

    public void persist(final @NotNull Session session) {
        sessionMap.put(session.getId(), session);
    }
}
