package ru.kombarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.ITaskEndpoint;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.enumerated.Status;
import ru.kombarov.tm.service.SessionService;
import ru.kombarov.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kombarov.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private TaskService taskService;

    public TaskEndpoint() {
        super();
    }

    public TaskEndpoint(final @NotNull SessionService sessionService, final @NotNull TaskService taskService) {
        super(sessionService);
        this.taskService = taskService;
    }

    @Override
    @WebMethod
    public void persistTask(final @NotNull Session session, final @Nullable Task task) throws Exception {
        validateSession(session);
        taskService.persist(task);
    }

    @Override
    @WebMethod
    public void persistTasks(final @NotNull Session session, final @Nullable List<Task> tasks) throws Exception {
        validateSession(session);
        taskService.persist(tasks);
    }

    @Override
    @WebMethod
    public void mergeTask(final @NotNull Session session, final @Nullable Task task) throws Exception {
        validateSession(session);
        taskService.merge(task);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findAllTasks(final @NotNull Session session) throws Exception {
        validateSession(session);
        return taskService.findAll();
    }

    @Nullable
    @Override
    @WebMethod
    public Task findOneTask(final @NotNull Session session, final @Nullable String id) throws Exception {
        validateSession(session);
        return taskService.findOne(id);
    }

    @Override
    @WebMethod
    public void removeTask(final @NotNull Session session, final @Nullable String id) throws Exception {
        validateSession(session);
        taskService.remove(id);
    }

    @Override
    @WebMethod
    public void removeAllTasks(final @NotNull Session session) throws Exception {
        validateSession(session);
        taskService.removeAll();
    }

    @Nullable
    @Override
    @WebMethod
    public String getIdByTaskName(final @NotNull Session session, final @Nullable String name) throws Exception {
        validateSession(session);
        return taskService.getIdByName(name);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> getTaskListByProjectId(final @NotNull Session session, final @Nullable String projectId) throws Exception {
        validateSession(session);
        return taskService.getTasksByProjectId(projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> getTaskListByUserId(final @NotNull Session session) throws Exception {
        validateSession(session);
        return taskService.getTasksByUserId(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findAllTasksByUserId(final @NotNull Session session) throws Exception {
        validateSession(session);
        return taskService.findAll(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Task findOneTaskByUserId(final @NotNull Session session, final @Nullable String id) throws Exception {
        validateSession(session);
        return taskService.findOne(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeAllTasksByUserId(final @NotNull Session session) throws Exception {
        validateSession(session);
        taskService.removeAll(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> sortTasksByDateStart(final @NotNull Session session) throws Exception {
        validateSession(session);
        return taskService.sortByDateStart(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> sortTasksByDateFinish(final @NotNull Session session) throws Exception {
        validateSession(session);
        return taskService.sortByDateFinish(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> sortTasksByStatus(final @NotNull Session session, final @NotNull Status status) throws Exception {
        validateSession(session);
        return taskService.sortByStatus(status, session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findTasksByPart(final @NotNull Session session, final @Nullable String part) throws Exception {
        validateSession(session);
        return taskService.findByPart(part, session.getUserId());
    }
}
