package ru.kombarov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.service.SessionService;

import java.util.Date;

import static ru.kombarov.tm.constant.AppConstant.*;
import static ru.kombarov.tm.util.SignatureUtil.sign;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    SessionService sessionService;

    public AbstractEndpoint(final @NotNull SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void validateSession(@Nullable final Session userSession) throws Exception {
        if(userSession == null) throw new Exception("Method is unavailable for unauthorized users");
        final @Nullable Session session = sessionService.findOne(userSession.getUserId(), userSession.getId());
        userSession.setSignature(null);
        if(session == null) throw new Exception("Session does not exist");
        if(!session.getSignature().equals(sign(userSession, SALT, CICLE))) throw new Exception("Session is invalid");
        if(new Date().getTime() - userSession.getDateCreate().getTime() > SESSION_LIFE_TIME) throw new Exception("Session time is out, you need to log in again");
    }
}
