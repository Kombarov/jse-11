package ru.kombarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.IUserEndpoint;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.enumerated.Role;
import ru.kombarov.tm.service.SessionService;
import ru.kombarov.tm.service.UserService;
import ru.kombarov.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

import static ru.kombarov.tm.constant.AppConstant.CICLE;
import static ru.kombarov.tm.constant.AppConstant.SALT;

@WebService(endpointInterface = "ru.kombarov.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private UserService userService;

    public UserEndpoint() {

    }

    public UserEndpoint(final @NotNull SessionService sessionService, final @NotNull UserService userService) {
        super(sessionService);
        this.userService = userService;
    }

    @Override
    @WebMethod
    public void persistUser(final @NotNull String login, final @NotNull String password, final @NotNull Role role) throws Exception {
        User user = new User();
        user.setLogin(login);
        user.setPassword(SignatureUtil.sign(password, SALT, CICLE));
        user.setRole(role);
        userService.persist(user);
    }

    @Override
    @WebMethod
    public void persistUsers(final @Nullable Session session, final @Nullable List<User> users) throws Exception {
        validateSession(session);
        userService.persist(users);
    }

    @Override
    @WebMethod
    public void mergeUser(final @Nullable Session session, final @Nullable User user) throws Exception {
        validateSession(session);
        userService.merge(user);
    }

    @NotNull
    @Override
    @WebMethod
    public List<User> findAllUsers(final @Nullable Session session) throws Exception {
        return userService.findAll();
    }

    @Nullable
    @Override
    @WebMethod
    public User findOneUser(final @Nullable Session session, final @Nullable String id) throws Exception {
        validateSession(session);
        return userService.findOne(id);
    }

    @Override
    @WebMethod
    public void removeUser(final @Nullable Session session, final @Nullable String id) throws Exception {
        validateSession(session);
        userService.remove(id);
    }

    @Override
    @WebMethod
    public void removeAllUsers(final @Nullable Session session) throws Exception {
        validateSession(session);
        userService.removeAll();
    }

    @Nullable
    @Override
    @WebMethod
    public String getIdByUserName(final @Nullable Session session, final @Nullable String name) throws Exception {
        validateSession(session);
        return userService.getIdByName(name);
    }

    @NotNull
    @Override
    @WebMethod
    public List<User> findAllUsersByUserId(final @Nullable Session session, final @Nullable String userId) throws Exception {
        validateSession(session);
        return userService.findAll(userId);
    }

    @Nullable
    @Override
    @WebMethod
    public User findOneUserByUserId(final @Nullable Session session, final @Nullable String userId, final @Nullable String id) throws Exception {
        validateSession(session);
        return userService.findOne(userId, id);
    }

    @Override
    @WebMethod
    public void removeAllUsersByUserId(final @Nullable Session session, final @Nullable String userId) throws Exception {
        validateSession(session);
        userService.removeAll(userId);
    }

    @Override
    @WebMethod
    public boolean checkUserLogin(final @Nullable Session session, final @Nullable String login) throws Exception {
        validateSession(session);
        return userService.checkLogin(login);
    }

    @Override
    @WebMethod
    public void loadDataBin(final @Nullable Session session) throws Exception {
        validateSession(session);
        userService.loadDataBin();
    }

    @Override
    @WebMethod
    public void loadDataJsonByFasterXml(final @Nullable Session session) throws Exception {
        validateSession(session);
        userService.loadDataJsonByFasterXml();
    }

    @Override
    @WebMethod
    public void loadDataXmlByFasterXml(final @Nullable Session session) throws Exception {
        validateSession(session);
        userService.loadDataXmlByFasterXml();
    }

    @Override
    @WebMethod
    public void loadDataJsonByJaxB(final @Nullable Session session) throws Exception {
        validateSession(session);
        userService.loadDataJsonByJaxB();
    }

    @Override
    @WebMethod
    public void loadDataXmlByJaxB(final @Nullable Session session) throws Exception {
        validateSession(session);
        userService.loadDataXmlByJaxB();
    }

    @Override
    @WebMethod
    public void saveDataBin(final @Nullable Session session) throws Exception {
        validateSession(session);
        userService.saveDataBin();
    }

    @Override
    @WebMethod
    public void saveDataJsonByFasterXml(final @Nullable Session session) throws Exception {
        validateSession(session);
        userService.saveDataJsonByFasterXml();
    }

    @Override
    @WebMethod
    public void saveDataXmlByFasterXml(final @Nullable Session session) throws Exception {
        validateSession(session);
        userService.saveDataXmlByFasterXml();
    }

    @Override
    @WebMethod
    public void saveDataJsonByJaxB(final @Nullable Session session) throws Exception {
        validateSession(session);
        userService.saveDataJsonByJaxB();
    }

    @Override
    @WebMethod
    public void saveDataXmlByJaxB(final @Nullable Session session) throws Exception {
        validateSession(session);
        userService.saveDataXmlByJaxB();
    }
}
