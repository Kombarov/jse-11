package ru.kombarov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.api.ServiceLocator;
import ru.kombarov.tm.endpoint.ProjectEndpoint;
import ru.kombarov.tm.endpoint.SessionEndpoint;
import ru.kombarov.tm.endpoint.TaskEndpoint;
import ru.kombarov.tm.endpoint.UserEndpoint;
import ru.kombarov.tm.repository.ProjectRepository;
import ru.kombarov.tm.repository.SessionRepository;
import ru.kombarov.tm.repository.TaskRepository;
import ru.kombarov.tm.repository.UserRepository;
import ru.kombarov.tm.service.ProjectService;
import ru.kombarov.tm.service.SessionService;
import ru.kombarov.tm.service.TaskService;
import ru.kombarov.tm.service.UserService;

import javax.xml.ws.Endpoint;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final TaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final UserRepository userRepository = new UserRepository();

    @NotNull
    private final SessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final ProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final TaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final UserService userService = new UserService(userRepository, projectService, taskService);

    @Getter
    @NotNull
    private final SessionService sessionService = new SessionService(sessionRepository);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(sessionService, projectService);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(sessionService, taskService);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(sessionService, userService);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService, userService);

    public void init() throws Exception {
        Endpoint.publish("http://localhost:8080/projectService?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/taskService?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/userService?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/sessionService?wsdl", sessionEndpoint);
    }
}