package ru.kombarov.tm.datatransfer;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.ServiceLocator;
import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.service.ProjectService;
import ru.kombarov.tm.service.TaskService;
import ru.kombarov.tm.service.UserService;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement
public class DataTransfer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Nullable
    @XmlElement
    private List<User> users;

    @Getter
    @Nullable
    @XmlElement
    private List<Project> projects;

    @Getter
    @Nullable
    @XmlElement
    private List<Task> tasks;

    public void load(final @NotNull ProjectService projectService, final @NotNull TaskService taskService, final @NotNull UserService userService) throws Exception {
        @Nullable
        final User currentUser = userService.getUserCurrent();
        if (currentUser == null) throw new Exception();
        this.users = new LinkedList<>(userService.findAll());
        this.projects = new LinkedList<>(projectService.findAll(currentUser.getId()));
        this.tasks = new LinkedList<>(taskService.findAll(currentUser.getId()));
    }
}
